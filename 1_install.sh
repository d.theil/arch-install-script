#!/bin/bash

# ----------------------
# Installer script for Arch Linux
# ----------------------
# UEFI based systems only

script_dir="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
error_invalid="Error: please select a valid option!"
int_regex="^[0-9]+$"
disk_regex=".*?[0-9]$"

# ----------------------
# Functions
# ----------------------

choose_disk () {
    if [ $1 -gt 0 ]
    then
        echo "Select disk you want to setup the partitions on by number(Between 0 and $1): "
    else
        echo "Select disk you want to setup the partitions on by number(Example: 0): "
    fi
    read -r DISKCHOICE
    if [[ (${DISKCHOICE} -eq 0 || ${DISKCHOICE} -gt 0) && (${DISKCHOICE} -eq ${1} || ${DISKCHOICE} -lt ${1}) && (${DISKCHOICE} =~ ${int_regex}) ]]
    then
        chosen_disk=$DISKCHOICE
    else
        echo ${error_invalid}
        choose_disk ${1}
    fi
}

swap_partition () {
    echo "Would you like to add a swap partition? (Y/N):"
    read -r SWAPCHOICE

    case ${SWAPCHOICE} in
        [Yy]* ) 
            echo "Select the size for your swap partition in GB (Default: 4GB):"
            read -r SWAPSIZE
            if [[ -z ${SWAPSIZE} ]]
            then
                SWAPSIZE=4
            fi
            if [[ (${SWAPSIZE} -eq 1 || ${SWAPSIZE} -gt 1) && (${SWAPSIZE} =~ ${int_regex}) ]]
            then
                # Creating a swap partition
                sgdisk -n 2:0:+${SWAPSIZE}GB "${1}"
                sgdisk -t 2:8200 "${1}"
                sgdisk -c 2:'swap' "${1}"
                root_disk_number=3
            else
                echo "${error_invalid}"
                swap_partition $1
            fi
            ;;
        [Nn]* ) 
            root_disk_number=2
            ;;
        * ) 
            echo "${error_invalid}"
            swap_partition $1
            ;;
    esac

}

echo "WARNING: Running this script will format your disk, are you sure you want to continue? (Y/N):"
read -r RUNSCRIPT
case ${RUNSCRIPT} in
    [Yy]* ) 
        # Update system clock
        timedatectl set-ntp true

        # ----------------------
        # Setting up mirrors for pacman
        # ----------------------

        # Synchronizing database for pacman
        pacman -Sy
        # Download reflector and rsync to update the mirrorlist
        pacman -S --noconfirm reflector rsync
        # Backup mirrorlist in case reflector doesn't work for whatever reason
        mv /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup
        # Check closest and fastest mirrors. Sort mirrors by speed and save them to the mirrorlist file
        reflector --sort rate -l 20 --save /etc/pacman.d/mirrorlist

        # ----------------------
        # Partition disk/disks
        # ----------------------

        # Get disk names
        disks=()
        IFS=, read -r -a array <<< $(lsblk -dn -o NAME|tr '\n' , )
        for disk in "${array[@]}"
        do
            disks+=("/dev/$disk")
        done 
        disk_number="$(( ${#disks[@]}-1 ))"
        for i in "${!disks[@]}"; do
            echo "$i) ${disks[${i}]}"
        done
        # Choose disk to create the partitions on
        choose_disk ${disk_number}

        disk_name="${disks[$chosen_disk]}"
        # Clear the disk of any data structures remaining
        sgdisk -Z "${disk_name}"
        # Set alignment to 2048 and clear disk
        sgdisk -a 2048 -o "${disk_name}"

        # Creating UEFI partition
        sgdisk -n 1:0:+550M "${disk_name}"
        sgdisk -t 1:ef00 "${disk_name}"
        sgdisk -c 1:"UEFI" "${disk_name}"

        # Ask if you would like a swap partition
        eval swap_partition "${disk_name}"

        # Creating the root partition for Arch
        eval sgdisk -n ${root_disk_number}:0:0 "${disk_name}"
        eval sgdisk -t ${root_disk_number}:8300 "${disk_name}"
        eval sgdisk -c ${root_disk_number}:"arch" "${disk_name}"
        
        # Check if disk name ends with a number
        if [[ ${disk_name} =~ ${disk_regex} ]]
        then
            disk_p="p"
        else
            disk_p=""
        fi

        eval mkfs.vfat -F32 "${disk_name}${disk_p}1"
        if [[ $root_disk_number -eq 3 ]]
        then
            eval mkswap "${disk_name}${disk_p}2"
            eval swapon "${disk_name}${disk_p}2"
        fi
        eval mkfs.ext4 "${disk_name}${disk_p}${root_disk_number}"

        # # ----------------------
        # # Installation Arch
        # # ----------------------
        mkdir /mnt
        # Mount the disk to start installing Arch
        eval mount "${disk_name}${disk_p}${root_disk_number}" /mnt

        # Install basic packages
        pacstrap /mnt base base-devel linux-lts linux-firmware
        # Generate fstab, partition definition table
        genfstab -U /mnt >> /mnt/etc/fstab
        # Copy the script to the main installation
        cp -R ${script_dir} /mnt/root/arch-install-script
        # Copy the updated mirrorlist to the Arch install
        cp /etc/pacman.d/mirrorlist /mnt/etc/pacman.d/mirrorlist

        # run 2_configure.sh when you're in the install to continue
    ;;
esac