#!/bin/bash

bash 1_install.sh
arch-chroot /mnt /bin/bash /root/arch-install-script/2_configure.sh
umount -R /mnt
reboot