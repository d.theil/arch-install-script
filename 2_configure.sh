#!/bin/bash

# ----------------------
# Configure the system
# ----------------------

# ----------------------
# Functions
# ----------------------

pc_speaker () {
    echo "Would you like to turn off your PC speaker? (In case the sound is really loud) (Y/N):"
    read -r PCSPEAKER

    case ${PCSPEAKER} in
        [Yy]* ) 
            touch /etc/modprobe.d/nobeep.conf
            echo "blacklist pcspkr" > /etc/modprobe.d/nobeep.conf
            ;;
    esac
}

# ----------------------
# Network
# ----------------------

# Install networkmanager for a internet connection
pacman -S --noconfirm networkmanager dhclient
systemctl enable NetworkManager 

# ----------------------
# Boot Loader
# ----------------------
# Gets the CPU brand
if [[ "$(lscpu)" =~ "GenuineIntel" ]]
then
    cpu_ucode="intel-ucode"
elif [[ "$(lscpu)" =~ "AuthenticAMD" ]]
then
    cpu_ucode="amd-ucode"
else
    cpu_ucode=""
fi

# Download and install GRUB as the boot loader
eval pacman -S --noconfirm grub efibootmgr "${cpu_ucode}"
mkdir /boot/efi
mount /dev/sda1 /boot/efi
grub-install --target=x86_64-efi  --bootloader-id=GRUB --efi-directory=/boot/efi
grub-mkconfig -o /boot/grub/grub.cfg

# ----------------------
# Time zone
# ----------------------

ln -sf /usr/share/zoneinfo/Europe/Amsterdam /etc/localtime
hwclock --systohc

# ----------------------
# Localization
# ----------------------

# Set the locale
sed -i 's/^#en_GB.UTF-8 UTF-8/en_GB.UTF-8 UTF-8/' /etc/locale.gen
locale-gen
localectl --no-ask-password set-locale en_GB.UTF-8
# Set keymap
localectl --no-ask-password set-keymap us

# ----------------------
# Hostname
# ----------------------

# Sets the name for the machine 
echo "Please enter a name for this machine:" 
read -r ARCHNAME
echo $ARCHNAME > /etc/hostname

# ----------------------
# Root password
# ----------------------
echo "Please enter a password for root:"
passwd

# ----------------------
# Turn off PC speaker
# ----------------------

pc_speaker

# ----------------------
# End script
# ----------------------

exit