# Arch linux install script
Personalised Arch install script. 
For UEFI systems only

## Arch ISO
Run the following commands to run the install script while in the live install environment

```
pacman -Sy git
git clone https://gitlab.com/d.theil/arch-install-script
cd arch-install-script
sh start.sh
```